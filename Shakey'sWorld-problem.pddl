(define (problem world-of-shakey-problem)
    (:domain world-of-shakey-domain)
    (:objects
        box1    box2    box3    box4
        pos1    pos2    pos3    pos4    pos5    pos6    pos7    pos8
        under1  under2  under3  under4
        bot
        switch1    switch2    switch3    switch4    
    )
    (:init
        (robot bot)
        (pos pos1)  (pos pos2)  (pos pos3)  (pos pos4)  (pos pos5) (pos pos6)   (pos pos7)  (pos pos8)
        (switch switch1)    (switch switch2)    (switch switch3)    (switch switch4)
        (at switch1 under1) (at switch2 under2) (at switch3 under3) (at switch4 under4)
        (pos under1)    (pos under2)    (pos under3)    (pos under4)
        (box box1)  (box box2)  (box box3)  (box box4)
        (at box1 pos1)  (at box2 pos1)  (at box3 pos1)  (at box4 pos1)

        (near pos1 pos5)    (near pos5 pos1)
        (near pos2 pos6)    (near pos6 pos2)
        (near pos3 pos7)    (near pos7 pos3)
        (near pos4 pos8)    (near pos8 pos4)
        (near pos5 pos6)
        (near pos6 pos5)    (near pos6 pos7)
        (near pos7 pos6)    (near pos7 pos8)
        (near pos8 pos7)
        (near under1 pos1)  (near pos1 under1)
        (near under2 pos2)  (near pos2 under2)
        (near under3 pos3)  (near pos3 under3)
        (near under4 pos4)  (near pos4 under4)
        
        (at bot pos2)
        (down bot)
        (on switch2)
    )
    (:goal (and
        (on switch1)
        (on switch2)
        (on switch3)
        (on switch4)
        (at box1 under1)
        (at box2 under2)
        (at box3 under3)
        (at box4 under4))
    )
)